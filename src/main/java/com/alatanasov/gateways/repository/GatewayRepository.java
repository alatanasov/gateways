package com.alatanasov.gateways.repository;

import com.alatanasov.gateways.model.Gateway;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GatewayRepository extends JpaRepository<Gateway, String> {
}
