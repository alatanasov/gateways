package com.alatanasov.gateways.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Gateway {
    @Id
    @Column(unique = true)
    private String serialNumber;
    private String gatewayName;
    private String ip;

    @OneToMany(mappedBy = "gateway", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Device> deviceList;

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getGatewayName() {
        return gatewayName;
    }

    public void setGatewayName(String gatewayName) {
        this.gatewayName = gatewayName;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public List<Device> getDeviceList() {
        return deviceList;
    }

    public void setDeviceList(List<Device> deviceList) {
        this.deviceList = deviceList;
    }

    @Override
    public String toString() {
        return "Gateway{" +
                "serialNumber='" + serialNumber + '\'' +
                ", gatewayName='" + gatewayName + '\'' +
                ", ip='" + ip + '\'' +
                '}';
    }
}
