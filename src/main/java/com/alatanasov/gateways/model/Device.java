package com.alatanasov.gateways.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@Entity
public class Device {
    @Id
    @GeneratedValue
    @Column(unique = true)
    private Long uid;
    private String vendor;
    protected Date dateCreated;
    private Status status;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "serialNumber")
    private Gateway gateway;

    public Device(String vendor, Status status) {
        this();
        this.vendor = vendor;
        this.status = status;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Gateway getGateway() {
        return gateway;
    }

    public void setGateway(Gateway gateway) {
        this.gateway = gateway;
    }

    public Device() {
        this.dateCreated = Calendar.getInstance().getTime();
    }

    @Override
    public String toString() {
        return "Device{" +
                "vendor='" + vendor + '\'' +
                ", status=" + status +
                '}';
    }

    public enum Status {
        online,
        offline
    }
}
