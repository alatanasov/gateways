package com.alatanasov.gateways.exception;

public class DeviceNotFoundException extends RuntimeException {

    public DeviceNotFoundException(Long uid) {
        super("Cannot find device " + uid);
    }
}