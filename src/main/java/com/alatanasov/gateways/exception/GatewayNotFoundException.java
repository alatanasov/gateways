package com.alatanasov.gateways.exception;

public class GatewayNotFoundException extends RuntimeException {

    public GatewayNotFoundException(String serialNumber) {
        super("Cannot find gateway " + serialNumber);
    }
}