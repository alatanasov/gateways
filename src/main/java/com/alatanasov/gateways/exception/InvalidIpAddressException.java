package com.alatanasov.gateways.exception;

public class InvalidIpAddressException extends RuntimeException {

    public InvalidIpAddressException(String ip) {
        super("The provided IP address is invalid!");
    }
}