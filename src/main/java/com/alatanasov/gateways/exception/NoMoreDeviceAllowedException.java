package com.alatanasov.gateways.exception;

public class NoMoreDeviceAllowedException extends RuntimeException {

    public NoMoreDeviceAllowedException() {
        super("You have reached the maximum limit allowed !");
    }
}