package com.alatanasov.gateways.rest;

import com.alatanasov.gateways.exception.DeviceNotFoundException;
import com.alatanasov.gateways.exception.GatewayNotFoundException;
import com.alatanasov.gateways.exception.InvalidIpAddressException;
import com.alatanasov.gateways.exception.NoMoreDeviceAllowedException;
import com.alatanasov.gateways.model.Device;
import com.alatanasov.gateways.model.Gateway;
import com.alatanasov.gateways.repository.GatewayRepository;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
public class GatewayResource {
    final GatewayRepository gatewayRepository;

    public GatewayResource(GatewayRepository gatewayRepository) {
        this.gatewayRepository = gatewayRepository;
    }

    //List all gateways
    @GetMapping(path = "/gateways", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Gateway> getAllGateways() {
        return gatewayRepository.findAll();
    }

    //List a single gateway
    @GetMapping(path = "/gateway/{serialNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Optional<Gateway> getGateway(@PathVariable("serialNumber") String serialNumber) {
        return Optional.ofNullable(gatewayRepository.findById(serialNumber)
                .orElseThrow(() -> new GatewayNotFoundException(serialNumber)));
    }

    //Add gateway
    @PostMapping(path = "/gateway")
    @ResponseBody
    public String addGateway(@RequestBody Gateway gateway) {
        if (!isIpValid(gateway.getIp())) throw new InvalidIpAddressException(gateway.getIp());
        gatewayRepository.save(gateway);
        return "A new gateway has been added";
    }

    //Delete gateway
    @DeleteMapping("/gateway/{serialNumber}")
    @ResponseBody
    public String deleteGateway(@PathVariable String serialNumber) {
        Gateway gateway = gatewayRepository.getOne(serialNumber);
        gatewayRepository.delete(gateway);
        return "The gateway has been deleted";
    }

    //Add device
    @PostMapping("/gateways/{serialNumber}/device")
    @ResponseBody
    public String addDevice(@RequestBody Device newDevice, @PathVariable String serialNumber) {
        gatewayRepository.findById(serialNumber)
                .map(gateway -> {
                    if (gateway.getDeviceList().size() == 10)
                        throw new NoMoreDeviceAllowedException();

                    Device device = new Device(newDevice.getVendor(), newDevice.getStatus());
                    device.setGateway(gateway);

                    gateway.getDeviceList().add(device);
                    return gatewayRepository.save(gateway);
                }).orElseThrow(() -> new GatewayNotFoundException(serialNumber));

        return "A device for " + serialNumber + " is created";
    }

    //Delete device
    @DeleteMapping("/gateways/{serialNumber}/device/{uid}")
    @ResponseBody
    public String deleteDevice(@PathVariable String serialNumber, @PathVariable Long uid) {
        gatewayRepository.findById(serialNumber)
                .map(gateway -> {
                    boolean removed = false;
                    for (int i = 0; i < gateway.getDeviceList().size(); i++) {
                        Device device = gateway.getDeviceList().get(i);
                        if (device.getUid().equals(uid)) {
                            gateway.getDeviceList().remove(i);
                            removed = true;
                            device.setGateway(null);
                            break;
                        }
                    }
                    if (!removed) throw new DeviceNotFoundException(uid);
                    return gatewayRepository.save(gateway);
                }).orElseThrow(() -> new GatewayNotFoundException(serialNumber));
        return "A device for " + serialNumber + " is deleted";
    }

    private boolean isIpValid(String ip) {
        Pattern pattern = Pattern.compile("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$");
        Matcher matcher = pattern.matcher(ip);
        return matcher.find();
    }
}
