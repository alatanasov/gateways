# Description
REST service (JSON/HTTP) for storing information about gateways and their associated devices.<br>

## Technologies used
- Programming language: Java
- Framework: Spring Boot
- Database: In-memory
- Automated build: Apache Maven

## Available Scripts
- To build and run project, open a console and execute ./build-and-start.sh

# Endpoints:

## List all existing gateways:
GET http://localhost:8080/gateways

## List a single gateway:
GET http://localhost:8080/gateway/{serialNumber} // ex: http://localhost:8080/gateway/Sn6

## Add gateway
POST http://localhost:8080/gateway <br>
body: {<br>
"serialNumber": "string", //a unique serial number ex: Sn6<br>
"gatewayName": "string", //a human-readable name ex: Gateway 6<br>
"ip": "string" //an IPv4 address ex: 10.0.0.6<br>
}

## Delete gateway:
DELETE http://localhost:8080/gateway/{serialNumber} // ex: http://localhost:8080/gateway/Sn6

## Add device
POST http://localhost:8080/gateways/{serialNumber}/device <br>
body: {<br>
"vendor": "string", // ex: Vd1<br>
"status": "online|offline" // ex: online<br>
}

## Delete device
DELETE http://localhost:8080/gateways/{serialNumber}/device/{uid} // ex: http://localhost:8080/gateways/Sn6/device/1

# Additional information
To access the H2 in memory database go to : 
http://localhost:8080/h2-console/login.jsp

Credentials : <br>
JDBC URL: jdbc:h2:mem:alatanasov 
<br>
Username: sa
<br>
Password: 
